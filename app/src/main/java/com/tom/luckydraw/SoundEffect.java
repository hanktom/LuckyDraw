package com.tom.luckydraw;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.preference.PreferenceManager;

import java.util.HashMap;
import java.util.Map;

public class SoundEffect {
    public static final int SOUND_DI = 1;
    public static final int SOUND_BON = 2;
    public static final int SOUND_CORRECT = 3;
    static SoundPool soundPool;
    static Context context;
    static Map<Integer, Integer> soundPoolMap = new HashMap<>();
    private SoundEffect(){

    }
    public static void init(Context context){
        SoundEffect.context = context;
        if (soundPool == null) {
            soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 1);
            soundPoolMap.put(SOUND_DI, soundPool.load(context, R.raw.di, 1));
            soundPoolMap.put(SOUND_BON, soundPool.load(context, R.raw.bon, 1));
            soundPoolMap.put(SOUND_CORRECT, soundPool.load(context, R.raw.correct, 1));
        }
    }

    public static void play(int sound){
        boolean soundEnabled = PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(context.getString(R.string.pref_sound_enable), true);
        if (soundEnabled) {
            soundPool.play(soundPoolMap.get(sound),
                    1, 1, 1, 0, 1);
        }
    }
}
