package com.tom.luckydraw;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private List<Applicant> applicants;
    private TextView nameText;
    private TextView totalText;
    private Spinner spinner;
    private List<String> lists;
    private BounceView bounceView;
    private TextView luckyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: token: " + FirebaseInstanceId.getInstance().getToken());
        SoundEffect.init(this);
        findViews();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideResult();
                new DrawingTask().execute(8);
                bounceView.setBallCount(applicants.size());
            }
        });
        //
        FirebaseDatabase.getInstance().getReference("lists")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        lists = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            lists.add(snapshot.getKey());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,
                                android.R.layout.simple_spinner_item, lists);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String listName = (String) spinner.getSelectedItem();
                readListFromFirebase(listName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
//        readListFromFirebase("listAll");
    }

    private void findViews() {
        nameText = findViewById(R.id.nameText);
        totalText = findViewById(R.id.totalText);
        spinner = findViewById(R.id.spinner);
        bounceView = findViewById(R.id.bounce);
        luckyText = findViewById(R.id.lucky);
    }

    private void readListFromFirebase(String listName) {
        applicants = new ArrayList<>();
        FirebaseDatabase.getInstance().getReference("lists")
                .child(listName)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            int no = -1;
                            String id = "";
                            try {
                                no = Integer.parseInt(snapshot.getKey());
                                id = (String)snapshot.getValue();
                            } catch (NumberFormatException e) {
                                id = snapshot.getKey();
                            }
                            Applicant applicant = new Applicant(no, id);
                            applicants.add(applicant);
                        }
                        totalText.setText(applicants.size()+"");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    class DrawingTask extends AsyncTask<Integer, Integer, Integer>{
        boolean stop = false;
        Random random = new Random();
        @Override
        protected Integer doInBackground(Integer... integers) {
            int duration = integers[0];
            int current = -1;
                    Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    stop = true;
                }
            }, duration*1000);
            while (!stop){
                current = random.nextInt(applicants.size());
                publishProgress(current);
            }
            return current;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            nameText.setText(applicants.get(integer).getId());
            FirebaseDatabase.getInstance()
                    .getReference("lucky")
                    .push()
                    .setValue(applicants.get(integer));
            SoundEffect.play(SoundEffect.SOUND_CORRECT);
            showResult(applicants.get(integer).getId());
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int index = values[0];
            nameText.setText(applicants.get(index).getId());
        }
    }

    private void showResult(String id) {
        bounceView.setBallCount(0);
        nameText.setVisibility(View.GONE);
        luckyText.setText(id);
        luckyText.setVisibility(View.VISIBLE);
    }

    private void hideResult() {
        nameText.setVisibility(View.VISIBLE);
        luckyText.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
