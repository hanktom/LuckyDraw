package com.tom.luckydraw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

public class BounceView extends View{

    private final Paint paint;
    Handler handler = new Handler();
    List<Ball> balls = new ArrayList<>();
    int ballCount = 0;
    private int radius = 40;
    private SoundPool soundPool;
    private int soundDi;

    public BounceView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        SoundEffect.init(context);
        paint = new Paint();
        post(new Runnable() {
            @Override
            public void run() {
                Ball.SCREEN_WIDTH = getWidth();
                Ball.SCREEN_HEIGHT = getHeight();
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for (Ball ball : balls) {
            for (Ball otherBall : balls) {
                if (ball == otherBall) continue;
                if (((ball.x-radius) <= otherBall.x && (ball.x+radius) >= otherBall.x) &&
                        ((ball.y-radius) <= otherBall.y && (ball.y+radius) >= otherBall.y)){
                    ball.setCollision(true);
                    otherBall.setCollision(true);
                    SoundEffect.play(SoundEffect.SOUND_DI);
                }
            }
            paint.setColor(ball.color);
            canvas.drawCircle(ball.x, ball.y, radius, paint);
            ball.move();
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                invalidate();
            }
        }, 50);
    }

    public int getBallCount() {
        return ballCount;
    }

    public void setBallCount(int ballCount) {
        this.ballCount = ballCount;
        balls.clear();
        for (int i = 0; i < ballCount; i++) {
            balls.add(new Ball());
        }
        invalidate();
    }
}
