package com.tom.luckydraw;

import android.graphics.Color;

import java.util.Random;

public class Ball {
    public static int SCREEN_WIDTH = 0;
    public static int SCREEN_HEIGHT = 0;
    int color;
    float x;
    float y;
    int tranX;
    int tranY;
    boolean collision = false;
    Random random = new Random();

    public Ball(){
        x = random.nextInt(SCREEN_WIDTH);
        y = random.nextInt(SCREEN_HEIGHT);
        randomTranXY();
        color = Color.argb(255,
                random.nextInt(256),
                random.nextInt(256),
                random.nextInt(256));
    }

    private void randomTranXY() {
        tranX = random.nextInt(100)-50;
        tranY = random.nextInt(100)-50;
    }

    public void move() {
        if (collision) {
            randomTranXY();
            collision = false;
        }
        x += tranX;
        y += tranY;
        if (x > SCREEN_WIDTH){
            x = SCREEN_WIDTH;
            tranX = -tranX;
        }else if (x < 0){
            x = 0;
            tranX = -tranX;
        }
        if (y > SCREEN_HEIGHT){
            y = SCREEN_HEIGHT;
            tranY = -tranY;
        }else if (y < 0){
            y = 0;
            tranY = -tranY;
        }
    }

    public boolean isCollision() {
        return collision;
    }

    public void setCollision(boolean collision) {
        this.collision = collision;
    }
}
