package com.tom.luckydraw;

public class Applicant {
    int no;
    String id;

    public Applicant() {
    }

    public Applicant(int no, String id) {
        this.no = no;
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
